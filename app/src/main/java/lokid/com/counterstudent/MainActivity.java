package lokid.com.counterstudent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Integer counterStudents = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    /**
     * Увеличение счётчика студентов на 1
     * @param view - объект {@link View}, в котором происходит событие
     */
    public void onClickButtonAddStudents(View view) {
        counterStudents++;
        outputCountOfStudents();
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", counterStudents);
    }



    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null &&
                savedInstanceState.containsKey("counter")) {
            counterStudents = savedInstanceState.getInt("counter");
            outputCountOfStudents();
        }
    }


    public void outputCountOfStudents() {
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", counterStudents));
    }
}
